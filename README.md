# chmod_all

Pythonファイルとシェルファイルに実行権限を与えるツールです。

あなたはパッケージを入れた時にファイルへの実行権限の付与が面倒に感じませんか？

このツールを使えばcatkin_wsフォルダ以下のすべてのPythonファイルとシェルファイルに簡単に実行権限を与えることができます。



It is a tool that gives execute permission to Python files and shell files.

Do you feel troublesome to grant execute permission to the file when you put in the package?

With this tool you can easily grant execute permission to all Python files and shell files under the catkin_ws folder.


## preparation commands:

```bash
$ cd ~
$ git clone https://gitlab.com/YuasaDaiki/chmod_all.git
$ cd chmod_all
$ chmod 755 *.py
```

## 簡単に実行できるようにbashrcにコマンドを追加する
まずはbashrcを開く。
```bash
$ gedit ~/.bashrc
```
以下の行をファイルの最後に追加して保存する。
```bash
alias ch='python ~/chmod_all/chmod_all.py '
```

端末を新しく開き直し、「ch」と入力すれば実行される。ただし、端末の階層はホームディレクトリより下の階層で行うこと。（ルートディレクトリではうまく動かない。）